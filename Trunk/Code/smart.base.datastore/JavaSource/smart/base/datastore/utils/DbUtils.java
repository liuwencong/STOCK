package smart.base.datastore.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Map.Entry;

public class DbUtils {
  public static final String CHARSET_UTF_8 = "UTF-8";

  public static Connection getConnection(String jdbcUrl, Map<String, String> params) throws SQLException, UnsupportedEncodingException {
    StringBuilder builder = new StringBuilder();
    builder.append(jdbcUrl);
    int count = 0;
    for (Entry<String, String> entry : params.entrySet()) {
      String key = entry.getKey();
      String value = URLEncoder.encode(entry.getValue(), CHARSET_UTF_8);
      if (count == 0) {
        builder.append("?");
      } else {
        builder.append("&");
      }
      builder.append(key + "=" + value);
      count++;
    }
    Connection conn = DriverManager.getConnection(builder.toString());
    return conn;
  }

  public static void closeConnection(Connection conn) throws SQLException {
    if (conn != null) {
      conn.close();
    }
  }

  public static void closeStatement(Statement stmt) throws SQLException {
    if (stmt != null) {
      stmt.close();
    }
  }

  public static void closeResultSet(ResultSet rs) throws SQLException {
    if (rs != null) {
      rs.close();
    }
  }
}
