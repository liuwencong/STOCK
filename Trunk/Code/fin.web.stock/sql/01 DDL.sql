-- 说明：该脚本用来归档DDL SQL脚本 ----------------------------------------------

-- --------------------------------------------[2020-11-14] lwc start----------------------------------------------
-- 创建database
create database if not exists dwi;
create database if not exists dwrfin;
create database if not exists dmfin;
create database if not exists sys;

-- 股票代码补录表
USE DWI;
DROP TABLE IF EXISTS `SDI_STOCK_CODE`;
CREATE TABLE `SDI_STOCK_CODE` (
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 中国A股市场股票代码，将本地股票代码导入数据库表，主键',
  `IS_ACTIVE` varchar(1) COMMENT '是否有效: 默认Y，其中Y-有效，N-失效',
  `CREATED_BY` decimal(38,0) COMMENT '创建人: 默认值-1',
  `CREATION_DATE` datetime COMMENT '创建时间: 默认值0000-00-00 00:00:00',
  `LAST_UPDATED_BY` decimal(38,0) COMMENT '最后修改人: 默认值-1',
  `LAST_UPDATE_DATE` datetime COMMENT '最后修改时间: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (STOCK_CODE)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `SDI_STOCK_HISTORY_PRICE_0001`;
CREATE TABLE `SDI_STOCK_HISTORY_PRICE_0001` (
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 股票市场对应各个公司股票代码，股票编码为接口入参',
  `DATE` date COMMENT '日期: 股票交易日期，时间粒度到天，接口示例：http://quotes.money.163.com/service/chddata.html?code=0600000',
  `NAME` varchar(100) COMMENT '名称: 股票名称',
  `CLOSING_PRICE` decimal(38,9) COMMENT '收盘价: 当日股票收盘价',
  `HIGHEST_PRICE` decimal(38,9) COMMENT '最高价: 当日股票最高价',
  `LOWEST_PRICE` decimal(38,9) COMMENT '最低价: 当日股票最低价',
  `OPENING_PRICE` decimal(38,9) COMMENT '开盘价: 当日股票开盘价',
  `BEF_CLOSING` decimal(38,9) COMMENT '前收盘: 前一日股票收盘价',
  `CHANGE_AMT` decimal(38,9) COMMENT '涨跌额: 涨跌额 = 收盘价 - 前收盘',
  `CHANGE_PCT` decimal(38,9) COMMENT '涨跌幅: 涨跌幅 = 100 * (收盘价 - 前收盘) / 前收盘，实际含义为-1.174%，单位为百分比',
  `TURNOVER_RATE` decimal(38,9) COMMENT '换手率: 实际值为0.191%，单位为百分比',
  `DEAL_QTY` decimal(38,0) COMMENT '成交量: 当日股票成交总股数，单位为股',
  `DEAL_AMT` decimal(38,9) COMMENT '成交金额: 当日股票成交总金额，单位为人民币',
  `TOTAL_MARKET_VAL` decimal(38,9) COMMENT '总市值: 当日上市公司总市值，根据收盘价统计，单位为人民币',
  `CIRCULATION_MARKET_VAL` decimal(38,9) COMMENT '流通市值: 当日上市公司流通股数总市值，根据收盘价统计，单位为人民币',
  `NUM_OF_TRANS` decimal(38,0) COMMENT '成交笔数',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `SDI_STOCK_REAL_TIME_PRICE_0002`;
CREATE TABLE `SDI_STOCK_REAL_TIME_PRICE_0002` (
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 股票编码为接口入参',
  `STOCK_NAME` varchar(100) COMMENT '股票名字: 接口示例：http://hq.sinajs.cn/list=sh600000，接口返回值含义：https://blog.csdn.net/focusjava/article/details/50394021',
  `TODAY_OPENING_PRICE` decimal(38,9) COMMENT '今日开盘价: 今开',
  `YESTERDAY_CLOSING_PRICE` decimal(38,9) COMMENT '昨日收盘价: 昨收',
  `CURRENT_PRICE` decimal(38,9) COMMENT '当前价格: 最新价',
  `TODAY_HIGHEST_PRICE` decimal(38,9) COMMENT '今日最高价: 最高',
  `TODAY_LOWEST_PRICE` decimal(38,9) COMMENT '今日最低价: 最低',
  `BID_PRICE` decimal(38,9) COMMENT '竞买价: 竞买价，即“买一”报价',
  `AUCTION_PRICE` decimal(38,9) COMMENT '竞卖价: 竞卖价，即“卖一”报价',
  `DEAL_QTY` decimal(38,0) COMMENT '成交量: 成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百',
  `DEAL_AMT` decimal(38,9) COMMENT '成交额: 成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万',
  `BUY_1_QTY` decimal(38,0) COMMENT '买一股数: “买一”股数，申请308710股，即3087手',
  `BUY_1_PRICE` decimal(38,9) COMMENT '买一报价: “买一”报价',
  `BUY_2_QTY` decimal(38,0) COMMENT '买二股数: “买二”股数',
  `BUY_2_PRICE` decimal(38,9) COMMENT '买二报价: “买二”报价',
  `BUY_3_QTY` decimal(38,0) COMMENT '买三股数: “买三”股数',
  `BUY_3_PRICE` decimal(38,9) COMMENT '买三报价: “买三”报价',
  `BUY_4_QTY` decimal(38,0) COMMENT '买四股数: “买四”股数',
  `BUY_4_PRICE` decimal(38,9) COMMENT '买四报价: “买四”报价',
  `BUY_5_QTY` decimal(38,0) COMMENT '买五股数: “买五”股数',
  `BUY_5_PRICE` decimal(38,9) COMMENT '买五报价: “买五”报价',
  `SELL_1_QTY` decimal(38,0) COMMENT '卖一股数: “卖一”股数',
  `SELL_1_PRICE` decimal(38,9) COMMENT '卖一报价: “卖一”报价',
  `SELL_2_QTY` decimal(38,0) COMMENT '卖二股数: “卖二”股数',
  `SELL_2_PRICE` decimal(38,9) COMMENT '卖二报价: “卖二”报价',
  `SELL_3_QTY` decimal(38,0) COMMENT '卖三股数: “卖三”股数',
  `SELL_3_PRICE` decimal(38,9) COMMENT '卖三报价: “卖三”报价',
  `SELL_4_QTY` decimal(38,0) COMMENT '卖四股数: “卖四”股数',
  `SELL_4_PRICE` decimal(38,9) COMMENT '卖四报价: “卖四”报价',
  `SELL_5_QTY` decimal(38,0) COMMENT '卖五股数: “卖五”股数',
  `SELL_5_PRICE` decimal(38,9) COMMENT '卖五报价: “卖五”报价',
  `DATE` date COMMENT '日期: 日期，年月日格式，时间粒度到天',
  `TIME` time COMMENT '时间: 时间，时分秒格式，时间粒度到秒',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

use DWRFIN;
DROP TABLE IF EXISTS `DWR_DIM_FIN_STOCK_CODE_D`;
CREATE TABLE `DWR_DIM_FIN_STOCK_CODE_D` (
  `STOCK_CODE_KEY` decimal(38,0) COMMENT '股票代码key: 1、代理主键，编号从1000开始，中国A股市场有3000多只股票，根据数据总量确定开始编号；2、所有维表都要有两行空值处理记录，避免事实表中维度置空值。一行是key值为“-999999”，一行是“-999998”，处理事实维度为空、事实记录不存在、维度迟到的场景。',
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 中国A股市场股票代码，业务主键',
  `STOCK_NAME` varchar(100) COMMENT '股票名称: 股票名称，来源于网易163股票接口',
  `IS_ACTIVE` varchar(1) COMMENT '是否有效: Y-有效，N-失效',
  `SCD_ACTIVE_IND` decimal(38,0) COMMENT '缓慢变化有效标识: 0/1，其中1-有效，0-无效',
  `SCD_ACTIVE_BEGIN_DATE` datetime COMMENT '缓慢变化开始时间: 默认值1900/1/1',
  `SCD_ACTIVE_END_DATE` datetime COMMENT '缓慢变化结束时间: 默认值4721/12/31',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (STOCK_CODE_KEY),
  UNIQUE KEY (STOCK_CODE)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `DWD_FIN_STOCK_HISTORY_PRICE_F`;
CREATE TABLE `DWD_FIN_STOCK_HISTORY_PRICE_F` (
  `STATS_DATE` date COMMENT '统计日期: 股票交易日期，时间粒度到天，业务主键之一',
  `STOCK_CODE_KEY` decimal(38,0) COMMENT '股票代码key: 根据统计日期、股票代码查询对应股票代码key',
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 股票代码，业务主键之一',
  `STOCK_NAME` varchar(100) COMMENT '股票名称: 股票名称，记录股票历史名称',
  `CLOSING_PRICE` decimal(38,9) COMMENT '收盘价格: 当日股票收盘价',
  `HIGHEST_PRICE` decimal(38,9) COMMENT '最高价格: 当日股票最高价',
  `LOWEST_PRICE` decimal(38,9) COMMENT '最低价格: 当日股票最低价',
  `OPENING_PRICE` decimal(38,9) COMMENT '开盘价格: 当日股票开盘价',
  `YESTERDAY_CLOSING_PRICE` decimal(38,9) COMMENT '前日收盘价格: 前一日股票收盘价',
  `AVG_PRICE` decimal(38,9) COMMENT '平均价格: 平均价格=成交金额 / 成交量，当日成交均价',
  `CHANGE_AMT` decimal(38,9) COMMENT '涨跌额: 涨跌额 = 收盘价格 - 前日收盘价格',
  `CHANGE_PCT` decimal(38,5) COMMENT '涨跌幅: 涨跌幅 = (收盘价格 - 前日收盘价格) / 前日收盘价格，实际含义为-1.174%，单位为百分比',
  `TURNOVER_RATE` decimal(38,5) COMMENT '换手率: 实际含义为0.191%，单位为百分比',
  `DEAL_QTY` decimal(38,0) COMMENT '成交量: 当日股票成交总股数，单位为股',
  `TOTAL_QTY` decimal(38,0) COMMENT '总股数: 总股数 = 总市值 / 收盘价格，上市公司所有股数',
  `CIRCULATION_QTY` decimal(38,0) COMMENT '流通股数: 流通股数 = 流通市值 / 收盘价格，上市公司流通股数',
  `DEAL_AMT` decimal(38,9) COMMENT '成交金额: 当日股票成交总金额，单位为人民币',
  `TOTAL_MARKET_VAL` decimal(38,9) COMMENT '总市值: 当日上市公司总市值，根据收盘价统计，单位为人民币',
  `CIRCULATION_MARKET_VAL` decimal(38,9) COMMENT '流通市值: 当日上市公司流通股数总市值，根据收盘价统计，单位为人民币',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (STATS_DATE, STOCK_CODE)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `DWD_FIN_STK_REAL_TIME_PRICE_F`;
CREATE TABLE `DWD_FIN_STK_REAL_TIME_PRICE_F` (
  `STATS_TIME` datetime COMMENT '统计时间: 股票统计时间，时间粒度到秒，业务主键之一',
  `STOCK_CODE_KEY` decimal(38,0) COMMENT '股票代码key: 根据统计日期、股票代码查询对应股票代码key',
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 股票编码为接口入参，业务主键之一',
  `STOCK_NAME` varchar(100) COMMENT '股票名称: 股票名称，记录股票历史名称',
  `LATEST_PRICE` decimal(38,9) COMMENT '最新价格: 当日内截止统计时间的股票最新价格',
  `HIGHEST_PRICE` decimal(38,9) COMMENT '最高价格: 当日内截止统计时间的股票最高价格',
  `LOWEST_PRICE` decimal(38,9) COMMENT '最低价格: 当日内截止统计时间的股票最低价格',
  `OPENING_PRICE` decimal(38,9) COMMENT '开盘价格: 当日内股票开盘价格',
  `YESTERDAY_CLOSING_PRICE` decimal(38,9) COMMENT '前日收盘价格: 前一日股票收盘价',
  `AVG_PRICE` decimal(38,9) COMMENT '平均价格: 平均价格=成交金额 / 成交量，当日成交均价',
  `CHANGE_AMT` decimal(38,9) COMMENT '涨跌额: 涨跌额 = 最新价格 - 前日收盘价格',
  `CHANGE_PCT` decimal(38,9) COMMENT '涨跌幅: 涨跌幅 = (最新价格 - 前日收盘价格) / 前日收盘价格，单位为百分比',
  `DEAL_QTY` decimal(38,0) COMMENT '成交量: 成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百',
  `DEAL_AMT` decimal(38,9) COMMENT '成交金额: 成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万',
  `BID_PRICE` decimal(38,9) COMMENT '竞买价格: 竞买价格，即“买一”报价',
  `AUCTION_PRICE` decimal(38,9) COMMENT '竞卖价格: 竞卖价格，即“卖一”报价',
  `BUY_1_QTY` decimal(38,0) COMMENT '买一股数: “买一”股数，申请308710股，即3087手',
  `BUY_1_PRICE` decimal(38,9) COMMENT '买一报价: “买一”报价',
  `BUY_2_QTY` decimal(38,0) COMMENT '买二股数: “买二”股数',
  `BUY_2_PRICE` decimal(38,9) COMMENT '买二报价: “买二”报价',
  `BUY_3_QTY` decimal(38,0) COMMENT '买三股数: “买三”股数',
  `BUY_3_PRICE` decimal(38,9) COMMENT '买三报价: “买三”报价',
  `BUY_4_QTY` decimal(38,0) COMMENT '买四股数: “买四”股数',
  `BUY_4_PRICE` decimal(38,9) COMMENT '买四报价: “买四”报价',
  `BUY_5_QTY` decimal(38,0) COMMENT '买五股数: “买五”股数',
  `BUY_5_PRICE` decimal(38,9) COMMENT '买五报价: “买五”报价',
  `SELL_1_QTY` decimal(38,0) COMMENT '卖一股数: “卖一”股数',
  `SELL_1_PRICE` decimal(38,9) COMMENT '卖一报价: “卖一”报价',
  `SELL_2_QTY` decimal(38,0) COMMENT '卖二股数: “卖二”股数',
  `SELL_2_PRICE` decimal(38,9) COMMENT '卖二报价: “卖二”报价',
  `SELL_3_QTY` decimal(38,0) COMMENT '卖三股数: “卖三”股数',
  `SELL_3_PRICE` decimal(38,9) COMMENT '卖三报价: “卖三”报价',
  `SELL_4_QTY` decimal(38,0) COMMENT '卖四股数: “卖四”股数',
  `SELL_4_PRICE` decimal(38,9) COMMENT '卖四报价: “卖四”报价',
  `SELL_5_QTY` decimal(38,0) COMMENT '卖五股数: “卖五”股数',
  `SELL_5_PRICE` decimal(38,9) COMMENT '卖五报价: “卖五”报价',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (STATS_TIME, STOCK_CODE)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `DWS_FIN_STK_REAL_TIME_PRI_D_F`;
CREATE TABLE `DWS_FIN_STK_REAL_TIME_PRI_D_F` (
  `STATS_DATE` date COMMENT '统计日期: 时间粒度到天，业务主键之一，数据来源于股票历史价格事实表、股票实时价格事实表，当同一只股票两个表数据都有更新时，优先取股票历史价格事实表数据',
  `STOCK_CODE_KEY` decimal(38,0) COMMENT '股票代码key: 数据来源于股票历史价格事实表、股票实时价格事实表，当同一只股票两个表数据都有更新时，优先取股票历史价格事实表数据',
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 业务主键之一，数据来源于股票历史价格事实表、股票实时价格事实表，当同一只股票两个表数据都有更新时，优先取股票历史价格事实表数据',
  `STOCK_NAME` varchar(100) COMMENT '股票名称: 记录股票历史名称，数据来源于股票历史价格事实表、股票实时价格事实表，当同一只股票两个表数据都有更新时，优先取股票历史价格事实表数据',
  `LATEST_PRICE` decimal(38,9) COMMENT '最新价格: 当日内股票最新价格',
  `HIGHEST_PRICE` decimal(38,9) COMMENT '最高价格: 当日内股票最高价格',
  `LOWEST_PRICE` decimal(38,9) COMMENT '最低价格: 当日内股票最低价格',
  `OPENING_PRICE` decimal(38,9) COMMENT '开盘价格: 当日内股票开盘价格',
  `YESTERDAY_CLOSING_PRICE` decimal(38,9) COMMENT '前日收盘价格: 前一日股票收盘价格',
  `AVG_PRICE` decimal(38,9) COMMENT '平均价格: 平均价格=成交金额 / 成交量，当日成交均价',
  `CHANGE_AMT` decimal(38,9) COMMENT '涨跌额: 涨跌额 = 最新价格 - 前日收盘价格',
  `CHANGE_PCT` decimal(38,9) COMMENT '涨跌幅: 涨跌幅 = (最新价格 - 前日收盘价格) / 前日收盘价格，单位为百分比',
  `DEAL_QTY` decimal(38,0) COMMENT '成交量: 成交的股票数，由于股票交易以一百股为基本单位，所以在使用时，通常把该值除以一百',
  `DEAL_AMT` decimal(38,9) COMMENT '成交金额: 成交金额，单位为“元”，为了一目了然，通常以“万元”为成交金额的单位，所以通常把该值除以一万',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (STATS_DATE, STOCK_CODE)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

use DMFIN;
DROP TABLE IF EXISTS `DM_FIN_STK_REAL_TIME_PRICE_D_F`;
CREATE TABLE `DM_FIN_STK_REAL_TIME_PRICE_D_F` (
  `STATS_DATE` date COMMENT '统计日期: 时间粒度到天，业务主键之一',
  `STOCK_CODE` varchar(50) COMMENT '股票代码: 业务主键之一',
  `STOCK_NAME` varchar(100) COMMENT '股票名称: 记录股票历史名称',
  `CHANGE_PCT` decimal(38,9) COMMENT '涨跌幅: 涨跌幅 = (最新价格 - 前日收盘价格) / 前日收盘价格，单位为百分比',
  `LATEST_PRICE` decimal(38,9) COMMENT '最新价格: 当日内股票最新价格',
  `CHANGE_AMT` decimal(38,9) COMMENT '涨跌额: 涨跌额 = 最新价格 - 前日收盘价格',
  `YESTERDAY_CLOSING_PRICE` decimal(38,9) COMMENT '前日收盘价格: 前一日股票收盘价格',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (STATS_DATE, STOCK_CODE)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

use SYS;
DROP TABLE IF EXISTS `SYS_TASK_PARAM_T`;
CREATE TABLE `SYS_TASK_PARAM_T` (
  `TASK_NAME` varchar(50) COMMENT '任务名称: 系统后台任务名称，主键之一',
  `PARAM_NAME` varchar(50) COMMENT '参数名称: 系统后台任务相关参数名称，主键之一',
  `PARAM_VAL` varchar(50) COMMENT '参数值: 系统后台任务相关参数值',
  `REM` varchar(100) COMMENT '备注',
  `DEL_FLAG` varchar(1) COMMENT '逻辑删除标志: Y/N，标明数据是否进行了逻辑删除，Y为是，N为否，一般用在SDI/DWD，SDI打好删除标识',
  `CRT_CYCLE_ID` decimal(38,0) COMMENT '数据创建批号ID: 默认值-1',
  `LAST_UPD_CYCLE_ID` decimal(38,0) COMMENT '数据更新批号ID: 默认值-1',
  `CRT_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被创建时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `UPD_JOB_INSTANCE_ID` decimal(38,0) COMMENT '数据被更新时的任务ID: 默认值-1，内容包括JOB_ID和ETL时间',
  `DW_CREATION_DATE` datetime COMMENT '数据创建时间: 默认值0000-00-00 00:00:00',
  `DW_LAST_MODIFIED_DATE` datetime COMMENT '最后更新日期: 默认值0000-00-00 00:00:00',
  PRIMARY KEY (TASK_NAME, PARAM_NAME)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------[2020-11-14] lwc end------------------------------------------------