package fin.app.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import smart.base.job.BaseJob;

@RestController
@EnableAutoConfiguration
public class Example {
  @RequestMapping("/fin.app.job")
  String home() {
    String content = BaseJob.getHome();
    return content;
  }

  public static void main(String[] args) throws Exception {
    SpringApplication.run(Example.class, args);
  }
}