一、Spring资料
1、spring-boot-reference.pdf
https://docs.spring.io/spring-boot/docs/1.5.9.RELEASE/reference/pdf/spring-boot-reference.pdf
2、Maven安装包  --apache-maven-3.3.3-bin.zip
https://archive.apache.org/dist/maven/maven-3/3.3.3/binaries/
解压至D:\Program Files\apache-maven-3.3.3
增加环境变量MAVEN_HOME，值为“D:\Program Files\apache-maven-3.3.3”
修改PATH环境变量，增加%MAVEN_HOME%\bin
3、Maven参考资料
https://maven.apache.org/guides/getting-started/index.html
4、eclipse中配置maven
Perferences -> Maven -> Installations，添加D:\Program Files\apache-maven-3.3.3
5、命令行方式新建maven工程  --参见《spring-boot-reference.pdf》中“Part II. Getting started”章节
(1)查看java版本
java -version
(2)查看maven版本
mvn -v
(3)创建com.example.myproject目录(D:/tmp)，并新建pom.xml文件，内容如下：
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.example</groupId>
    <artifactId>com.example.myproject</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.9.RELEASE</version>
    </parent>
    <!-- Additional lines to be added here... -->

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
(4)com.example.myproject目录中，新建src/main/java/com/example/myproject/Example.java，内容如下
package com.example.myproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class Example {
  @RequestMapping("/")
  String home() {
    return "Hello World!";
  }

  public static void main(String[] args) throws Exception {
    SpringApplication.run(Example.class, args);
  }
}
(5)maven打包工程
mvn clean
mvn package
(6)maven运行工程
mvn spring-boot:run
(7)浏览器访问如下路径
http://localhost:8080
(8)jar包方式运行工程
java -jar target/com.example.myproject-0.0.1-SNAPSHOT.jar
(9)示例代码归档至com.example.myproject.rar

6、eclipse管理maven工程
(1)eclipse中选择Import... -> Existing Maven Projects，选择D:\tmp\com.example.myproject目录，点击Finish即可
(2)eclipse打包com.example.myproject工程
右键com.example.myproject工程，选择“Run As --> Run Configurations...”，在“Maven Build”下新建执行任务，参数配置如下
Name: com.example.myproject [package]
Base directory: D:\tmp\com.example.myproject
Goals: clean package
JRE VM Arguments: -Dmaven.multiModuleProjectDirectory="D:\Program Files\apache-maven-3.3.3"
最后点击Run即可
(3)eclipse运行com.example.myproject工程
右键com.example.myproject工程，选择“Run As --> Run Configurations...”，在“Maven Build”下新建执行任务，参数配置如下
Name: com.example.myproject [run]
Base directory: D:\tmp\com.example.myproject
Goals: spring-boot:run
JRE VM Arguments: -Dmaven.multiModuleProjectDirectory="D:\Program Files\apache-maven-3.3.3"
最后点击Run即可
(7)浏览器访问如下路径
http://localhost:8080

7、创建Spring Boot多工程
(1)Creating a Multi Module Spring Project
https://spring.io/guides/gs/multi-module/
(2)涉及命令
mvn clean
mvn package
mvn install
mvn spring-boot:run -pl application
(3)访问web页面
http://localhost:8080/

二、教程
1、Spring Boot框架入门教程（快速学习版）
http://c.biancheng.net/spring_boot/
2、Spring Boot教程汇总
http://www.springboot.wiki/
3、maven教程
https://www.runoob.com/maven/maven-tutorial.html
http://c.biancheng.net/maven/


三、工程结构
1、Job任务执行
smart.base.job
  |--fin.app.job

2、Job任务管理
smart.base.job.manager
  |--fin.app.job.manager